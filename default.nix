{ system ? builtins.currentSystem, nixpkgs ? <nixpkgs>
, pkgs ? import nixpkgs { } }: {

  modules = import ./modules; # NixOS modules
  overlays = import ./overlays; # nixpkgs overlays
  lib = import ./lib { inherit pkgs; }; # functions

  bcachefs-tools = pkgs.callPackage ./pkgs/bcachefs-tools { };
  linux_sbos = pkgs.callPackage ./pkgs/linux_sbos {
    kernel = pkgs.linuxKernel.kernels.linux_5_19;
    kernelPatches = [
      pkgs.kernelPatches.bridge_stp_helper
      pkgs.kernelPatches.request_key_helper
    ];
  };

  libsigrok_pico = pkgs.callPackage ./pkgs/libsigrok_pico { };
  pulseview_pico = pkgs.callPackage ./pkgs/pulseview_pico { };
  cockpit = pkgs.callPackage ./pkgs/cockpit { };
  libvirt-dbus = pkgs.callPackage ./pkgs/libvirt-dbus { };
}
