# SBOSpkgs
##########################################
##          Copyleft 2022 FedX          ##
##  Mozilla Public License Version 2.0  ##
##  ==================================  ##
##########################################
##  THIS SOFTWARE IS PROVIDED "AS IS"   ##
##     WITHOUT WARRANTY OF ANY KIND     ##
##########################################
{ lib, fetchurl, kernel, kernelPatches, ... }@args:
# This is bsaed on yellowonion's bcachefs kernel patchset
# You can grab that in the NUR if you so wish.
# I am also using the security patchset from nixpkgs.
# Many of these options were taken from Zen/Liquorix as well.
with lib;

let
  commit = "4b20f394112fb3984662028735b3b5249a56c82f";
  diffHash = "1ddf7hfv8yiixrdil8hqmscm0ldni395hq7i6pq4v9lcz2r5l5by";
  shorthash = lib.strings.substring 0 7 commit;
  kernelVersion = kernel.version;
  oldPatches = kernelPatches;
in (kernel.override (args // {
  argsOverride = {
    version = "${kernelVersion}-SBOS-unstable-${shorthash}";
    extraMeta.branch = versions.majorMinor kernelVersion;

  } // (args.argsOverride or { });

  kernelPatches = [{
    name = "SBOS-config";
    patch = fetchurl {
      name = "bcachefs-${commit}.diff";
      url =
        "https://evilpiepirate.org/git/bcachefs.git/rawdiff/?id=${commit}&id2=v${
          lib.versions.majorMinor kernelVersion
        }";
      sha256 = diffHash;
    };
    # This patchset is a careful balence of optimizations. I am using the budget fair scheduler (BFQ) which runs more
    # efficently on desktop systems. I have patched this with BcahceFS a bleeding edge filesystem with improved speed
    # and sucurity for desktop systems. I have ensured under-used under tested, and potentialy insecure filesystems
    # are not utalized. This lowers build times, improves security, and remove unnessisary parts of the kernel. I
    # Have also enabled ZSWAP or the ability for the system to compress the RAM on a device. This also enables
    # SELinux by default, which may or may not be a terrible idea. This also has Android BinderFS support for
    # Waydroid support.
  }] ++ oldPatches;
  F2FS = no;
  BUG = yes;
  KFENCE = yes;
  KUNIT_DEBUGFS = yes;
  COMPACTION = yes;
  ZSWAP = yes;
  MQ_IOSCHED_DEADLINE = yes;
  IOSCHED_BFQ = yes;
  BCACHEFS_FS = yes;
  NTFS_FS = no;
  ADFS_FS = no;
  AFFS_FS = no;
  BFS_FS = no;
  BEFS_FS = no;
  CRAMFS = no;
  EROFS_FS = no;
  VXFS_FS = no;
  HFS_FS = no;
  HPFS_FS = no;
  JFS_FS = no;
  MINIX_FS = no;
  NILFS2_FS = no;
  OMFS_FS = no;
  QNX4FS_FS = no;
  QNX6FS_FS = no;
  SYSV_FS = no;
  UFS_FS = no;
  EXT2_FS = no;
  EXT3_FS = no;
  XFS_FS = no;
  OCFS2_FS = no;
  AFS_FS = no;
  UBIFS_FS = no;
  JFFS2_FS = no;
  HFSPLUS_FS = no;
  ECRYPT_FS = no;
  AFF_FS = no;
  CEF_FS = no;
  ANDROID_BINDERFS = yes;
  ###########
  # SELinux #
  ###########
  SECURITY_SELINUX = yes;
  SECURITY_SELINUX_BOOTPARAM = no;
  SECURITY_SELINUX_DISABLE = no;
  SECURITY_SELINUX_DEVELOP = yes;
  SECURITY_SELINUX_AVC_STATS = yes;
  SECURITY_SELINUX_CHECKREQPROT_VALUE = 0;
  DEFAULT_SECURITY_SELINUX = no;
  SECURITY_WRITABLE_HOOKS = no;

  GCC_PLUGINS = yes; # Enable gcc plugin options
  DEBUG_CREDENTIALS = yes;
  SLUB_DEBUG = yes;

  # Disable various dangerous settings
  ACPI_CUSTOM_METHOD = no; # Allows writing directly to physical memory
  PROC_KCORE = no; # Exposes kernel text image layout
  INET_DIAG = no; # Has been used for heap based attacks in the past

  ignoreConfigErrors = true;

  # TODO figure out how to remove F2FS from the build process.
}))
