{
  description = " My personal NUR repository";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  outputs = { self, nixpkgs }:
    let
      systems = [
        "x86_64-linux"
        "i686-linux"
        "x86_64-darwin"
        "aarch64-linux"
        "armv6l-linux"
        "armv7l-linux"
      ];
      forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f system);
      nixpkgsFor = forAllSystems (system:
        import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        });
    in {
      overlay = final: prev: {
        ################
        # Cockpit Base #
        ################
        cockpit = with final;
          stdenv.mkDerivation rec {
            pname = "cockpit";
            version = "278";

            src = fetchzip {
              url =
                "https://github.com/cockpit-project/cockpit/releases/download/${version}/cockpit-${version}.tar.xz";
              sha256 = "sha256-tcPzwLgVKnyiPNQubZ3ko+fRiFZINsw06ICjrf6ZjcI=";
            };
            configureFlags = [
              "--disable-pcp"
              "--disable-doc"
              "--with-systemdunitdir=$(out)/lib/systemd/system"
              "--sysconfdir=/etc"
            ];
            nativeBuildInputs = with pkgs; [
              pkg-config
              python3
              gnused
              makeWrapper
            ];
            buildInputs = [
              glib
              systemd
              json-glib
              gnutls
              krb5
              polkit
              libssh
              pam
              libxslt
              xmlto
              libxcrypt
            ];
            postPatch = ''
              patchShebangs tools
              sed -r '/^cmd_make_package_lock_json\b/ a exit 0' -i tools/node-modules
              substituteInPlace Makefile.in \
                --replace "\$(DESTDIR)\$(sysconfdir)" "$out/etc"
              substituteInPlace src/session/session-utils.h \
                --replace "DEFAULT_PATH \"" "DEFAULT_PATH \"${path}:"
            '';
            postInstall = ''
              wrapProgram "$out/libexec/cockpit-certificate-helper" \
                --suffix PATH : "${lib.makeBinPath [ coreutils openssl ]}"
            '';
            dontWrapGApps = true;
            pythonPath = with python3Packages; [ pygobject3 ];
            # TODO Figure out what the heck "client" is, and potentialy enable that.
          };
        ################
        # Libvirt-dbus #
        ################
        libvirt-dbus = with final;
          stdenv.mkDerivation rec {
            pname = "libvirt-dbus";
            version = "1.4.1";

            src = fetchFromGitLab {
              owner = "libvirt";
              repo = "libvirt-dbus";
              rev = "v${version}";
              sha256 =
                "sha256:112jbkp2b0pk6dpb0p68zg1ba196f4i0y57s1jzjn5vl4f11fv3g";
            };

            mesonFlags = [ "-Dsystem_user=root" ];

            nativeBuildInputs = [ meson pkg-config docutils ninja ];
            buildInputs = [ glib libvirt libvirt-glib ];

            meta = with lib; {
              description = "DBus protocol binding for libvirt native C API";
              license = licenses.lgpl2Plus;
              homepage = src.meta.homepage;
              platforms = platforms.linux;
              maintainers = with maintainers; [ ];
            };
          };
        ####################
        # Cokcpit Machines #
        ####################
        cockpit-machines = with final;
          stdenv.mkDerivation rec {
            pname = "cockpit-machines";
            version = "276";

            src = fetchzip {
              url =
                "https://github.com/cockpit-project/cockpit-machines/releases/download/${version}/cockpit-machines-${version}.tar.xz";
              sha256 = "";
            };

            nativeBuildInputs = [ gettext ];

            postPatch = ''
              substituteInPlace Makefile \
                --replace /usr/share $out/share
              touch pkg/lib/cockpit.js
              touch dist/manifest.json
            '';

            dontBuild = true;

            meta = with lib; {
              description = "Cockpit UI for virtual machines";
              license = licenses.lgpl21;
              homepage = "https://github.com/cockpit-project/cockpit-machines";
              platforms = platforms.linux;
              maintainers = with maintainers; [ ];
            };
          };
      };
      packages = forAllSystems (system: {
        inherit (nixpkgsFor.${system}) cockpit;
        inherit (nixpkgsFor.${system}) libvirt-dbus;
      });
      nixosModules.cockpit = { pkgs, lib, config, ... }:
        with lib;
        let cfg = config.services.cockpit;
        in {
          nixpkgs.overlays = [ self.overlay ];
          options.services.cockpit = {
            enable = mkEnableOption ''
              Cockpit web-based graphical interface for servers
            '';
            port = mkOption {
              type = types.port;
              default = 9090;
            };
          };

          config = mkIf cfg.enable {
            systemd.packages = with pkgs;
              [ (cockpit.override { packages = with pkgs; [ virtmanager ]; }) ];
            systemd.sockets.cockpit.wantedBy = [ "sockets.target" ];

            system.activationScripts = {
              cockpit = ''
                mkdir -p /etc/cockpit/ws-certs.d
                chmod 755 /etc/cockpit/ws-certs.d
              '';
            };

            security.pam.services.cockpit = { };

            environment.systemPackages = with pkgs; [
              cockpit
              cockpit-machines
              libvirt-dbus
            ];
            environment.pathsToLink = [ "/share/cockpit" ];

            systemd.sockets.cockpit.listenStreams =
              [ "" "${toString cfg.port}" ];
          };
        };
    };
}
